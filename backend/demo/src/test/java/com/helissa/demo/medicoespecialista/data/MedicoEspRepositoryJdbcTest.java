package com.helissa.demo.medicoespecialista.data;

import com.helissa.demo.enfermero.data.EnfermeroRepositoryjdbc;
import com.helissa.demo.enfermero.model.Enfermero;
import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MedicoEspRepositoryJdbcTest {



    MedicoEspRepositoryJdbc medicoEspRepositoryJdbc;
    @Before
    public void setUp() throws Exception {
        DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("create.sql"));
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        medicoEspRepositoryJdbc=new MedicoEspRepositoryJdbc(jdbcTemplate);

    }


    @Test
    public void load_all_medico_esp() throws SQLException {
        try {
            Collection<MedicoEsp> medico= medicoEspRepositoryJdbc.findAll();
            assertThat(medico,is(Arrays.asList(
                    new MedicoEsp(1, "123456789","Messi","0414712589412","12","cirugia"),
                    new MedicoEsp(2, "12345678910","El bicho","04147136373","12","cirugia")
            )));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void load_medico_esp_by_id() {
        MedicoEsp medicoEsp=medicoEspRepositoryJdbc.findById(1);
        assertThat(medicoEsp,is(new MedicoEsp(1, "123456789","Messi","0414712589412","12","cirugia")));
    }

    @Test
    public void insert_medico_esp() {
        MedicoEsp medicoEsp=new MedicoEsp(3, "123456789","Messi","0414712589412","12","cirugia");
        medicoEspRepositoryJdbc.saveOrUpdate(medicoEsp);
        MedicoEsp medicoActual=medicoEspRepositoryJdbc.findById(3);
        assertThat(medicoEsp,is(medicoActual));
    }





}