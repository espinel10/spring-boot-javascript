package com.helissa.demo.medicogeneral.data;

import com.helissa.demo.medicoespecialista.data.MedicoEspRepositoryJdbc;
import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import com.helissa.demo.medicogeneral.model.MedicoGen;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class MedicoGenRepositoryJdbcTest {


    MedicoGenRepositoryJdbc medicoGenRepositoryJdbc;
    @Before
    public void setUp() throws Exception {
        DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("create.sql"));
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        medicoGenRepositoryJdbc=new MedicoGenRepositoryJdbc(jdbcTemplate);

    }


    @Test
    public void load_all_medico_gen() throws SQLException {
        try {
            Collection<MedicoGen> medico= medicoGenRepositoryJdbc.findAll();
            assertThat(medico,is(Arrays.asList(
                    new MedicoGen(1, "123456789","Messi","0414712589412","12","cirugia"),
                    new MedicoGen(2, "12345678910","El bicho","04147136373","12","cirugia")
            )));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void load_medico_gen_by_id() {
        MedicoGen medicoGen=medicoGenRepositoryJdbc.findById(1);
        assertThat(medicoGen,is(new MedicoGen(1, "123456789","Messi","0414712589412","12","cirugia")));
    }


    @Test
    public void insert_medico_gen() {
        MedicoGen medicogen=new MedicoGen(3, "123456789","Messi","0414712589412","12","cirugia");
        medicoGenRepositoryJdbc.saveOrUpdate(medicogen);
        MedicoGen medicoActual=medicoGenRepositoryJdbc.findById(3);
        assertThat(medicogen,is(medicoActual));
    }




}