package com.helissa.demo.enfermero.data;

import com.helissa.demo.enfermero.model.Enfermero;
import com.helissa.demo.paciente.data.PacienteRepositoryjdbc;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import static org.hamcrest.CoreMatchers.is;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class EnfermeroRepositoryjdbcTest {
    EnfermeroRepositoryjdbc enfermeroRepositoryjdbc;
    @Before
    public void setUp() throws Exception {
        DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("create.sql"));
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        enfermeroRepositoryjdbc=new EnfermeroRepositoryjdbc(jdbcTemplate);

    }



    @Test
    public void load_all_enfermeros() throws SQLException {
        try {
            Collection<Enfermero> employes= enfermeroRepositoryjdbc.findAll();

            assertThat(employes,is(Arrays.asList(
                    new Enfermero(1, "123456789","Messi","0414712589412","12","cirugia"),
                    new Enfermero(2, "12345678910","El bicho","04147136373","12","cirugia")
            )));

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void load_enfermero_by_id() {
        Enfermero enfermero=enfermeroRepositoryjdbc.findById(1);
        assertThat(enfermero,is(new Enfermero(1, "123456789","Messi","0414712589412","12","cirugia")));
    }


    @Test
    public void insert_enfermero() {
        Enfermero enfermero=new Enfermero(3, "123456789","Messi","0414712589412","12","cirugia");
        enfermeroRepositoryjdbc.saveOrUpdate(enfermero);
        Enfermero enfermeroActual=enfermeroRepositoryjdbc.findById(3);
        assertThat(enfermero,is(enfermeroActual));
    }


}