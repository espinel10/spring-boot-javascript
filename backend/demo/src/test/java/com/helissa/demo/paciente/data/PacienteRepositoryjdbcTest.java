package com.helissa.demo.paciente.data;

import com.helissa.demo.paciente.model.Paciente;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import static org.hamcrest.CoreMatchers.is;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

public class PacienteRepositoryjdbcTest {

    PacienteRepositoryjdbc pacienteRepositoryjdbc;

    @Before
    public void setUp() throws Exception {
        DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
        ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("create.sql"));
        JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
        pacienteRepositoryjdbc=new PacienteRepositoryjdbc(jdbcTemplate);

    }


    @Test
    public void load_all_pacientes() {
        try {
            Collection<Paciente> employes= pacienteRepositoryjdbc.findAll();
            assertThat(employes,is(Arrays.asList(
                    new Paciente(1, "123456789","Messi","0414712589412"),
                    new Paciente(2, "12345678910","El bicho","04147136373")
            )));
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    @Test
    public void load_employe_by_id() {
        Paciente paciente=pacienteRepositoryjdbc.findById(1);
        assertThat(paciente,is(new Paciente(1, "123456789","Messi","0414712589412")));
    }

    @Test
    public void insert_a_paciente() {
        Paciente paciente=new Paciente(3,"456565656565","la pulga","78965478965");
        pacienteRepositoryjdbc.saveOrUpdate(paciente);
        Paciente actual=pacienteRepositoryjdbc.findById(3);
        assertThat(actual,is(paciente));
    }
}