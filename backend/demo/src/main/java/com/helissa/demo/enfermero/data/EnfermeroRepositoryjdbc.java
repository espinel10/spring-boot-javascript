package com.helissa.demo.enfermero.data;

import com.helissa.demo.enfermero.model.Enfermero;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class EnfermeroRepositoryjdbc implements EnfermeroRepository{
    JdbcTemplate jdbcTemplate;

    public EnfermeroRepositoryjdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate=jdbcTemplate;
    }

    @Override
    public Enfermero findById(long id) {
        Object[] args = { id };

        return jdbcTemplate.queryForObject("SELECT * FROM enfermero where id = ?",args,enfermeroRowMapper);

    }

    @Override
    public Collection<Enfermero> findAll() {
        return jdbcTemplate.query("select * from enfermero",enfermeroRowMapper);
    }

    @Override
    public void saveOrUpdate(Enfermero e) {
    jdbcTemplate.update("INSERT INTO enfermero (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES (?,?,?,?,?);",e.numeroI,e.nombre,e.telefono,e.tarjetaPro,e.especialidad);
    }



    private static RowMapper<Enfermero> enfermeroRowMapper = (rs, rowNum) ->
            new Enfermero(
                    rs.getInt("id"),
                    rs.getString("numeroI"),
                    rs.getString("nombre"),
                    rs.getString("telefono"),
                    rs.getString("tarjetaPro"),
                    rs.getString("especialidad")
            );



}
