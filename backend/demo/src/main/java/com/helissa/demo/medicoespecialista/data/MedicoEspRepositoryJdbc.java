package com.helissa.demo.medicoespecialista.data;

import com.helissa.demo.enfermero.model.Enfermero;
import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class MedicoEspRepositoryJdbc implements MedicoEspRepository{
    JdbcTemplate jdbcTemplate;

    public MedicoEspRepositoryJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate=jdbcTemplate;
    }

    @Override
    public MedicoEsp findById(long id) {
        Object[] args = { id };

        return jdbcTemplate.queryForObject("SELECT * FROM medico_esp where id = ?",args,medicoespRowMapper);

    }

    @Override
    public Collection<MedicoEsp> findAll() {
        return jdbcTemplate.query("select * from medico_esp",medicoespRowMapper);
    }

    @Override
    public void saveOrUpdate(MedicoEsp e) {
        jdbcTemplate.update("INSERT INTO medico_esp (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES (?,?,?,?,?);",e.numeroI,e.nombre,e.telefono,e.tarjetaPro,e.especialidad);
    }



    private static RowMapper<MedicoEsp> medicoespRowMapper = (rs, rowNum) ->
            new MedicoEsp(
                    rs.getInt("id"),
                    rs.getString("numeroI"),
                    rs.getString("nombre"),
                    rs.getString("telefono"),
                    rs.getString("tarjetaPro"),
                    rs.getString("especialidad")
            );
}
