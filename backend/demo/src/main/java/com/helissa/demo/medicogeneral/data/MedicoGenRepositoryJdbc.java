package com.helissa.demo.medicogeneral.data;

import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import com.helissa.demo.medicogeneral.model.MedicoGen;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class MedicoGenRepositoryJdbc implements MedicoGenRepository{
   JdbcTemplate jdbcTemplate;

    public MedicoGenRepositoryJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public MedicoGen findById(long id) {
        Object[] args = { id };
        return jdbcTemplate.queryForObject("SELECT * FROM medico_gen where id = ?",args,medicogenRowMapper);
    }

    @Override
    public Collection<MedicoGen> findAll() {
        return jdbcTemplate.query("select * from medico_gen",medicogenRowMapper);
    }

    @Override
    public void saveOrUpdate(MedicoGen e) {
        jdbcTemplate.update("INSERT INTO medico_gen (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES (?,?,?,?,?);",e.numeroI,e.nombre,e.telefono,e.tarjetaPro,e.especialidad);
    }


    private static RowMapper<MedicoGen> medicogenRowMapper = (rs, rowNum) ->
            new MedicoGen(
                    rs.getInt("id"),
                    rs.getString("numeroI"),
                    rs.getString("nombre"),
                    rs.getString("telefono"),
                    rs.getString("tarjetaPro"),
                    rs.getString("especialidad")
            );


}
