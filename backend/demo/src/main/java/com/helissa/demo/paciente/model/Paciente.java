package com.helissa.demo.paciente.model;

import java.util.Objects;

public class Paciente {
        public int id;
        public String numeroI;
        public String nombre;
        public String telefono;

    public Paciente() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paciente paciente = (Paciente) o;
        return id == paciente.id && Objects.equals(numeroI, paciente.numeroI) && Objects.equals(nombre, paciente.nombre) && Objects.equals(telefono, paciente.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numeroI, nombre, telefono);
    }

    @Override
    public String toString() {
        return "Paciente{" +
                "id=" + id +
                ", numeroI='" + numeroI + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNumeroI(String numeroI) {
        this.numeroI = numeroI;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public String getNumeroI() {
        return numeroI;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public Paciente(int id, String numeroI, String nombre, String telefono) {
        this.id = id;
        this.numeroI = numeroI;
        this.nombre = nombre;
        this.telefono = telefono;
    }
}
