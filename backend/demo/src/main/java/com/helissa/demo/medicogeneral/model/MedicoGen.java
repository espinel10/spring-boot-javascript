package com.helissa.demo.medicogeneral.model;

import java.util.Objects;

public class MedicoGen {
    public int id;
    public String numeroI;
    public String nombre;
    public String telefono;
    public String tarjetaPro;
    public String especialidad;

    @Override
    public String toString() {
        return "MedicoGen{" +
                "id=" + id +
                ", numeroI='" + numeroI + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", tarjetaPro='" + tarjetaPro + '\'' +
                ", especialidad='" + especialidad + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicoGen medicoGen = (MedicoGen) o;
        return id == medicoGen.id && Objects.equals(numeroI, medicoGen.numeroI) && Objects.equals(nombre, medicoGen.nombre) && Objects.equals(telefono, medicoGen.telefono) && Objects.equals(tarjetaPro, medicoGen.tarjetaPro) && Objects.equals(especialidad, medicoGen.especialidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numeroI, nombre, telefono, tarjetaPro, especialidad);
    }

    public MedicoGen(int id, String numeroI, String nombre, String telefono, String tarjetaPro, String especialidad) {
        this.id = id;
        this.numeroI = numeroI;
        this.nombre = nombre;
        this.telefono = telefono;
        this.tarjetaPro = tarjetaPro;
        this.especialidad = especialidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroI() {
        return numeroI;
    }

    public void setNumeroI(String numeroI) {
        this.numeroI = numeroI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTarjetaPro() {
        return tarjetaPro;
    }

    public void setTarjetaPro(String tarjetaPro) {
        this.tarjetaPro = tarjetaPro;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
}
