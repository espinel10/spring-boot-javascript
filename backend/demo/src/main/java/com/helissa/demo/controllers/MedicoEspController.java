package com.helissa.demo.controllers;

import com.helissa.demo.enfermero.data.EnfermeroRepositoryjdbc;
import com.helissa.demo.enfermero.model.Enfermero;
import com.helissa.demo.entities.BaseDatos;
import com.helissa.demo.medicoespecialista.data.MedicoEspRepositoryJdbc;
import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/helisa")
public class MedicoEspController {

    private BaseDatos bd=new BaseDatos();
    private MedicoEspRepositoryJdbc medicoEspRepositoryJdbc=new MedicoEspRepositoryJdbc(bd.getJdbcTemplate());

    @GetMapping("/medicoesp")
    @ApiOperation("Get all medicoesp")
    @ApiResponse(code=200,message = "OK")
    public Collection<MedicoEsp> getAllMedicoEsp(){
        return medicoEspRepositoryJdbc.findAll();
    }

    @GetMapping("medicoesp/{id}")
    @ApiOperation("Search a medico esp whit an ID")
    @ApiResponses(
            {
                    @ApiResponse(code=200,message = "OK"),
                    @ApiResponse(code=404,message = "medico esp not found")
            }
    )
    public ResponseEntity<MedicoEsp> geteMedicoEspById(@ApiParam(value="The id of the medico esp" ,required = true ,example = "1") @PathVariable(value = "id") Integer Id)  {
        MedicoEsp medicoEsp;
        medicoEsp=medicoEspRepositoryJdbc.findById(Id);
        return ResponseEntity.ok().body(medicoEsp);
    }


    @PostMapping("/medicoesp")
    public MedicoEsp createMedicoEsp(@Validated @RequestBody MedicoEsp medicoEsp){
        medicoEspRepositoryJdbc.saveOrUpdate(medicoEsp);
        return medicoEsp;
    }
}
