package com.helissa.demo.medicoespecialista.model;

import java.util.Objects;

public class MedicoEsp {
    public int id;
    public String numeroI;
    public String nombre;
    public String telefono;
    public String tarjetaPro;
    public String especialidad;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicoEsp medicoEsp = (MedicoEsp) o;
        return id == medicoEsp.id && Objects.equals(numeroI, medicoEsp.numeroI) && Objects.equals(nombre, medicoEsp.nombre) && Objects.equals(telefono, medicoEsp.telefono) && Objects.equals(tarjetaPro, medicoEsp.tarjetaPro) && Objects.equals(especialidad, medicoEsp.especialidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, numeroI, nombre, telefono, tarjetaPro, especialidad);
    }

    @Override
    public String toString() {
        return "MedicoEsp{" +
                "id=" + id +
                ", numeroI='" + numeroI + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono='" + telefono + '\'' +
                ", tarjetaPro='" + tarjetaPro + '\'' +
                ", especialidad='" + especialidad + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroI() {
        return numeroI;
    }

    public void setNumeroI(String numeroI) {
        this.numeroI = numeroI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTarjetaPro() {
        return tarjetaPro;
    }

    public void setTarjetaPro(String tarjetaPro) {
        this.tarjetaPro = tarjetaPro;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public MedicoEsp(int id, String numeroI, String nombre, String telefono, String tarjetaPro, String especialidad) {
        this.id = id;
        this.numeroI = numeroI;
        this.nombre = nombre;
        this.telefono = telefono;
        this.tarjetaPro = tarjetaPro;
        this.especialidad = especialidad;
    }
}
