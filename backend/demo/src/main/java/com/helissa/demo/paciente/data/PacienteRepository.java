package com.helissa.demo.paciente.data;

import com.helissa.demo.paciente.model.Paciente;

import java.util.Collection;

public interface PacienteRepository {
    Paciente findById(long id);
    Collection<Paciente> findAll();
    void saveOrUpdate(Paciente p);
}
