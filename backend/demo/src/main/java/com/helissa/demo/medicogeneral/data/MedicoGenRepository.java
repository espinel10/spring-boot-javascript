package com.helissa.demo.medicogeneral.data;

import com.helissa.demo.medicogeneral.model.MedicoGen;
import com.helissa.demo.paciente.model.Paciente;

import java.util.Collection;

public interface MedicoGenRepository {
    MedicoGen findById(long id);
    Collection<MedicoGen> findAll();
    void saveOrUpdate(MedicoGen e);

}
