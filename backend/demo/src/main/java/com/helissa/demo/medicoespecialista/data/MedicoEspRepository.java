package com.helissa.demo.medicoespecialista.data;

import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import com.helissa.demo.paciente.model.Paciente;

import java.util.Collection;

public interface MedicoEspRepository {

    MedicoEsp findById(long id);
    Collection<MedicoEsp> findAll();
    void saveOrUpdate(MedicoEsp p);
}
