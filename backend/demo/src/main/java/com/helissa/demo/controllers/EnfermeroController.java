package com.helissa.demo.controllers;


import com.helissa.demo.enfermero.data.EnfermeroRepositoryjdbc;
import com.helissa.demo.enfermero.model.Enfermero;
import com.helissa.demo.entities.BaseDatos;
import com.helissa.demo.paciente.data.PacienteRepositoryjdbc;
import com.helissa.demo.paciente.model.Paciente;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/helisa")
public class EnfermeroController {
    private BaseDatos bd=new BaseDatos();
    private EnfermeroRepositoryjdbc enfermeroRepositoryjdbc=new EnfermeroRepositoryjdbc(bd.getJdbcTemplate());

    @GetMapping("/enfermero")
    @ApiOperation("Get all enfermero")
    @ApiResponse(code=200,message = "OK")
    public Collection<Enfermero> getAllEnfermero(){
        return enfermeroRepositoryjdbc.findAll();
    }

    @GetMapping("enfermero/{id}")
    @ApiOperation("Search a enfermero whit an ID")
    @ApiResponses(
            {
                    @ApiResponse(code=200,message = "OK"),
                    @ApiResponse(code=404,message = "enfemero not found")
            }
    )
    public ResponseEntity<Enfermero> geteEnfermeroById(@ApiParam(value="The id of the enfermero" ,required = true ,example = "1") @PathVariable(value = "id") Integer Id)  {
        Enfermero enfermero;
        enfermero=enfermeroRepositoryjdbc.findById(Id);
        return ResponseEntity.ok().body(enfermero);
    }


    @PostMapping("/enfermero")
    public Enfermero createEnfermero(@Validated @RequestBody Enfermero enfermero){
        enfermeroRepositoryjdbc.saveOrUpdate(enfermero);
        return enfermero;
    }



}
