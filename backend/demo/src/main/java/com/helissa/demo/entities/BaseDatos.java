package com.helissa.demo.entities;

import com.helissa.demo.enfermero.data.EnfermeroRepositoryjdbc;
import com.helissa.demo.medicoespecialista.data.MedicoEspRepositoryJdbc;
import com.helissa.demo.medicogeneral.data.MedicoGenRepositoryJdbc;
import com.helissa.demo.paciente.model.Paciente;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

public class BaseDatos {
    private JdbcTemplate jdbcTemplate;
    public BaseDatos() {
        try {
            DriverManagerDataSource dataSource= new DriverManagerDataSource("jdbc:h2:mem:test;MODE=MYSQL", "sa", "sa");
            ScriptUtils.executeSqlScript(dataSource.getConnection(),new ClassPathResource("create.sql"));
            jdbcTemplate=new JdbcTemplate(dataSource);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
