package com.helissa.demo.enfermero.data;

import com.helissa.demo.enfermero.model.Enfermero;

import java.util.Collection;

public interface EnfermeroRepository {
    Enfermero findById(long id);
    Collection<Enfermero> findAll();
    void saveOrUpdate(Enfermero e);
}
