package com.helissa.demo.controllers;

import com.helissa.demo.entities.BaseDatos;
import com.helissa.demo.medicoespecialista.data.MedicoEspRepositoryJdbc;
import com.helissa.demo.medicoespecialista.model.MedicoEsp;
import com.helissa.demo.medicogeneral.data.MedicoGenRepositoryJdbc;
import com.helissa.demo.medicogeneral.model.MedicoGen;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/helisa")
public class MedicoGenController {
    private BaseDatos bd=new BaseDatos();
    private MedicoGenRepositoryJdbc medicoGenRepositoryJdbc=new MedicoGenRepositoryJdbc(bd.getJdbcTemplate());

    @GetMapping("/medicogen")
    @ApiOperation("Get all medicogen")
    @ApiResponse(code=200,message = "OK")
    public Collection<MedicoGen> getAllMedicoGen(){
        return medicoGenRepositoryJdbc.findAll();
    }

    @GetMapping("medicogen/{id}")
    @ApiOperation("Search a medico gen whit an ID")
    @ApiResponses(
            {
                    @ApiResponse(code=200,message = "OK"),
                    @ApiResponse(code=404,message = "medico gen not found")
            }
    )
    public ResponseEntity<MedicoGen> geteMedicoGenById(@ApiParam(value="The id of the medico gen" ,required = true ,example = "1") @PathVariable(value = "id") Integer Id)  {
        MedicoGen medicoGen;
        medicoGen=medicoGenRepositoryJdbc.findById(Id);
        return ResponseEntity.ok().body(medicoGen);
    }


    @PostMapping("/medicogen")
    public MedicoGen createMedicoGen(@Validated @RequestBody MedicoGen medicoGen){
        medicoGenRepositoryJdbc.saveOrUpdate(medicoGen);
        return medicoGen;
    }


}
