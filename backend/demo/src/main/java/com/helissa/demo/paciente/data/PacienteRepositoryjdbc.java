package com.helissa.demo.paciente.data;

import com.helissa.demo.paciente.model.Paciente;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.Collection;

public class PacienteRepositoryjdbc implements PacienteRepository{

    JdbcTemplate jdbcTemplate;
    private Log logger = LogFactory.getLog(PacienteRepositoryjdbc.class);
    public PacienteRepositoryjdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Paciente findById(long id) {
        Object[] args = { id };
        return jdbcTemplate.queryForObject("SELECT * FROM paciente where id = ?",args,pacienteMapper);
    }
    @Override
    public Collection<Paciente> findAll() {
        return jdbcTemplate.query("select * from paciente",pacienteMapper);
    }
    @Override
    public void saveOrUpdate(Paciente p) {
        jdbcTemplate.update("INSERT INTO paciente (numeroI,nombre,telefono) VALUES (?,?,?);",p.numeroI,p.nombre,p.telefono);
    }

    private static RowMapper<Paciente> pacienteMapper = (rs, rowNum) ->
            new Paciente(
                    rs.getInt("id"),
                    rs.getString("numeroI"),
                    rs.getString("nombre"),
                    rs.getString("telefono")
            );

}
