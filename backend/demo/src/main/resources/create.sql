CREATE TABLE IF NOT EXISTS paciente (
                                        id INT AUTO_INCREMENT PRIMARY KEY,
                                        numeroI VARCHAR(20) NOT NULL,
    nombre VARCHAR(20) NOT NULL,
    telefono VARCHAR(20) NOT NULL
    );

INSERT INTO paciente (numeroI,nombre,telefono) VALUES ('123456789','Messi','0414712589412'),('12345678910','El bicho','04147136373');

CREATE TABLE IF NOT EXISTS enfermero (
                                         id INT AUTO_INCREMENT PRIMARY KEY,
                                         numeroI VARCHAR(20) NOT NULL,
    nombre VARCHAR(20) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    tarjetaPro VARCHAR(20) NOT NULL,
    especialidad VARCHAR(20) NOT NULL
    );


INSERT INTO enfermero (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES  ('123456789','Messi','0414712589412','12','cirugia'),
                                                                                ('12345678910','El bicho','04147136373','12','cirugia');


CREATE TABLE IF NOT EXISTS medico_esp (
                                          id INT AUTO_INCREMENT PRIMARY KEY,
                                          numeroI VARCHAR(20) NOT NULL,
    nombre VARCHAR(20) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    tarjetaPro VARCHAR(20) NOT NULL,
    especialidad VARCHAR(20) NOT NULL
    );


INSERT INTO medico_esp (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES  ('123456789','Messi','0414712589412','12','cirugia'),
                                                                                 ('12345678910','El bicho','04147136373','12','cirugia');

CREATE TABLE IF NOT EXISTS medico_gen (
                                          id INT AUTO_INCREMENT PRIMARY KEY,
                                          numeroI VARCHAR(20) NOT NULL,
    nombre VARCHAR(20) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    tarjetaPro VARCHAR(20) NOT NULL,
    especialidad VARCHAR(20) NOT NULL
    );


INSERT INTO medico_gen (numeroI,nombre,telefono,tarjetaPro,especialidad) VALUES  ('123456789','Messi','0414712589412','12','cirugia'),
                                                                                 ('12345678910','El bicho','04147136373','12','cirugia');
